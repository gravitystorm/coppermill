class CreatePhotos < ActiveRecord::Migration[6.0]
  def change
    create_table :photos do |t|
      t.references :source, foreign_key: true
      t.string :object_key, null: false
      t.string :title, null: false
      t.integer :width, null: false
      t.integer :height, null: false
      t.string :description
      t.timestamps null: false
    end
  end
end
