class AddAccountMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :account_memberships do |t|
      t.references :user, foreign_key: true
      t.references :account, foreign_key: true
      t.timestamps null: false
    end
  end
end
