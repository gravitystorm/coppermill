class CreateSources < ActiveRecord::Migration[6.0]
  def change
    create_table :sources do |t|
      t.references :account, foreign_key: true
      t.string :name, null: false

      t.string :access_key, null: false
      t.string :secret_key, null: false
      t.string :host_base, null: false
      t.string :host_bucket, null: false
      t.string :bucket, null: false
      t.boolean :use_http, null: false, default: true

      t.timestamps null: false
    end
  end
end
