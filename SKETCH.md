# Structure

* There are accounts
* Accounts have many users. In reality there will be 1-5ish users per account, so that families can share access
* Users have many accounts. So that you can have one log in and have access to multiple accounts
* Accounts have many payment methods
* Accounts have one owner user
* Users have one profile
* Profiles belong to a user
* Accounts have many sources (places where photos are stored)
* Accounts have one scratchspace (place where resized photos are cached)
* Sources have many photos
* Photos belong to one source (TODO - should this be rethought for deduplication?)
* Accounts have many sets
* Sets have many photos
* Photos have zero or more sets
* (Sets can be shared)
* Photos have exif data (but perhaps this is an attribute)
* Photos have many tags
* Users have many friends. Two-way relationship, i.e. facebook not twitter
* Photos can have comments
* Sets can have comments

# Meta

* The app should be able to run on heroku, so that other people can deploy their own instances easily.
* You should be able to invite users to become your friends
* You should be able to put friends into groups (circles?) to share different sets of photos with them
* The system should be initially invite-only, to manage spam
* Later, each account can invite a small number of people to join the system. For example, 5 invites, refilling at 1 per week
* There needs to be some way to stop the system from being overwhelmed by spam
* We should make a list of different storage spaces that can be used, e.g. AWS S3, digitalocean spaces, exoscale, etc
* The aim should be to allow people to manage, curate and browse their own photos
