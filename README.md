# Coppermill

Coppermill is a web-based image management system. It takes all the bits of other sites that I like, gets rid of the bits that I don't like, and adds some new ideas.

## The big idea - bring your own storage!

We all have tens of thousands of photos. If we want them accessible outside of own homes, then we need to entrust them to some company, and upload them all to the company servers. Storing all those images costs money, so they are either expensive, or they play weird tricks with advertising or social media or whatnot. Also, these companies don't stick around forever, so at some point you get 3 months notice (if you're lucky) and you have to pick another service and re-upload all the files.

We have so many photos that it's impractical to store them all on a laptop or a desktop, so many people build their own NAS boxes at home. These are flakey and more expensive than you first realise, but you want to keep a resilient copy of your original photos. Eventually you realise having them all on one NAS in your own home is too dangerous, so you copy them all to cloud storage just in case.

Copppermill combines these two aspects of managing photos, by providing all the features of photo management websites, with read-only access to your cloud storage. You can configure a number of cloud storage providers, without having to upload anything, and since everything is open source you'll always be able to use the system that you've invested your time into.

## Development

The heart of the project is an open source ruby on rails project, found in this repository. Pull requests, issues etc are welcome.

## Naming

It's called coppermill purely because I bought a domain name a long time ago for a different idea that didn't happen. Maybe it'll change in the future, but finding good domain names is hard.

## License

The entire project is available under the Apache license.

Copyright 2020 Andy Allan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
