require 'mini_magick'

class ObjectImportJob < ApplicationJob
  # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
  def perform(source, key)
    return if source.photos.find_by(object_key: key)

    resp = source.client.get_object(
      bucket: source.bucket,
      key: key
    )

    unless resp.content_type == 'image/jpeg'
      log('not a photo')
      return
    end

    image = ::MiniMagick::Image.read(resp.body)

    photo = Photo.new(
      source: source,
      object_key: key,
      title: key,
      width: image.width,
      height: image.height
    )

    photo.save!

    resp.body.rewind
    photo.image.attach(io: resp.body, filename: key)
    log 'photo saved'
  end
  # rubocop:enable Metrics/AbcSize, Metrics/MethodLength
end
