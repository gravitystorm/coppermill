class SourceImportJob < ApplicationJob
  def perform(source)
    resp = source.client.list_objects(
      bucket: source.bucket,
      max_keys: 1000
    )

    resp.contents.each do |object|
      ObjectImportJob.perform_later(source, object.key)
    end
  end
end
