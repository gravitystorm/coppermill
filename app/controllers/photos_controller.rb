class PhotosController < ApplicationController
  authorize_resource

  before_action :load_account

  def show
    @photo = current_user.photos.find(params[:id])
  end

  private

  def load_account
    @account = Account.find(params[:account_id])
  end
end
