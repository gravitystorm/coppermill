class SourcesController < ApplicationController
  authorize_resource

  before_action :load_account

  def index; end

  def show
    @source = @account.sources.find(params[:id])
    @photos = @source.photos.order(id: 'desc').paginate(page: params[:page], per_page: 60)
  end

  def new
    @source = @account.sources.new
  end

  def edit
    @source = @account.sources.find(params[:id])
  end

  def create
    @source = @account.sources.new(source_params)

    if @source.save
      redirect_to @account
    else
      render :new
    end
  end

  def update
    @source = @account.sources.find(params[:id])

    if @source.update(source_params)
      flash_message(:success)
      redirect_to @account
    else
      render :edit
    end
  end

  def import
    @source = @account.sources.find(params[:source_id])
    SourceImportJob.perform_later(@source)
    flash_message(:success)
    redirect_to @account
  end

  private

  def load_account
    @account = Account.find(params[:account_id])
  end

  def source_params
    params.require(:source).permit(:name, :access_key, :secret_key, :host_base, :host_bucket, :bucket, :use_http)
  end
end
