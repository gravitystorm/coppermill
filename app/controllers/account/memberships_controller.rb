class Account::MembershipsController < ApplicationController
  authorize_resource class: false

  before_action :load_account

  def index; end

  private

  def load_account
    @account = Account.find(params[:account_id])
  end
end
