class AccountsController < ApplicationController
  authorize_resource

  before_action :load_account

  def show
    @photos = @account.photos.order(id: 'desc').paginate(page: params[:page], per_page: 60)
  end

  def edit; end

  def update
    if @account.update(account_params)
      flash_message(:success)
      redirect_to @account
    else
      flash_message(:failure)
      render :edit
    end
  end

  private

  def load_account
    @account = current_user.accounts.find(params[:id])
  end

  def account_params
    params.require(:account).permit(:name)
  end
end
