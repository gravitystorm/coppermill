class ApplicationController < ActionController::Base
  check_authorization unless: :devise_controller?

  def after_sign_in_path_for(resource)
    stored_location_for(resource) || dashboard_path
  end

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to main_app.root_url, notice: exception.message }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  def flash_message(type, options = {})
    flash_key = type == :success ? :notice : :alert
    options.reverse_merge!(scope: "#{controller_path.tr('/', '.')}.#{action_name}")
    flash[flash_key] = I18n.t(type, options) # rubocop:disable Rails/ActionControllerFlashBeforeRender
  end
end
