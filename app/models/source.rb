# == Schema Information
#
# Table name: sources
#
#  id          :bigint           not null, primary key
#  access_key  :string           not null
#  bucket      :string           not null
#  host_base   :string           not null
#  host_bucket :string           not null
#  name        :string           not null
#  secret_key  :string           not null
#  use_http    :boolean          default(TRUE), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  account_id  :bigint
#
# Indexes
#
#  index_sources_on_account_id  (account_id)
#
# Foreign Keys
#
#  fk_rails_...  (account_id => accounts.id)
#

class Source < ApplicationRecord
  validates :access_key, presence: true
  validates :bucket, presence: true
  validates :host_base, presence: true
  validates :host_bucket, presence: true
  validates :name, presence: true
  validates :secret_key, presence: true
  validates :use_http, presence: true

  belongs_to :account
  has_many :photos, dependent: :destroy

  def endpoint
    if use_http
      "http://#{host_base}"
    else
      "https://#{host_base}"
    end
  end

  def client
    @client ||= ::Aws::S3::Client.new(
      region: 'us-west-1',
      endpoint: endpoint,
      access_key_id: access_key,
      secret_access_key: secret_key
    )
  end
end
