# == Schema Information
#
# Table name: account_memberships
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  account_id :bigint
#  user_id    :bigint
#
# Indexes
#
#  index_account_memberships_on_account_id  (account_id)
#  index_account_memberships_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (account_id => accounts.id)
#  fk_rails_...  (user_id => users.id)
#

class AccountMembership < ApplicationRecord
  belongs_to :user
  belongs_to :account
end
