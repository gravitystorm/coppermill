Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  resource :dashboard
  resources :accounts do
    scope module: :account do
      resources :memberships, only: [:index]
    end
    resources :sources do
      post :import
    end
    resources :photos
  end
end
