# == Schema Information
#
# Table name: photos
#
#  id          :bigint           not null, primary key
#  description :string
#  height      :integer          not null
#  object_key  :string           not null
#  title       :string           not null
#  width       :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  source_id   :bigint
#
# Indexes
#
#  index_photos_on_source_id  (source_id)
#
# Foreign Keys
#
#  fk_rails_...  (source_id => sources.id)
#

FactoryBot.define do
  factory :photo do
    sequence(:title) { |n| "Photo Title #{n}" }
  end
end
