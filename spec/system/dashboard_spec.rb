require 'rails_helper'

RSpec.describe 'dashboard' do
  context 'when not signed in' do
    it 'does not work' do
      visit dashboard_path
      expect(page).to have_current_path(root_path)
      expect(page).to have_content('not authorized')
    end
  end

  context 'when signed in' do
    include_context 'when signed in as a site user'

    it 'shows your email address' do
      visit dashboard_path
      expect(page).to have_content(current_user.email)
    end
  end
end
