require 'rails_helper'

def confirm_from_email
  email = open_last_email_for('test@example.com')

  link_match = email.body.match(%r{href="[^"]+(/users/confirmation[^"]+)"})
  visit link_match[1]
end

RSpec.describe 'sign up' do
  it 'lets you sign up' do
    visit root_path
    click_on 'Sign Up'

    fill_in 'Email', with: 'test@example.com'
    fill_in 'Password', with: 'testtest'
    fill_in 'Password confirmation', with: 'testtest'
    click_on 'Sign up'

    expect(page).to have_current_path(root_path)
    expect(page).to have_content('A message with a confirmation link')

    confirm_from_email

    # should not be signed in
    expect(page).to have_content(I18n.t('.devise.confirmations.confirmed'))
    expect(page).to have_current_path(new_user_session_path)
  end
end
