require 'rails_helper'

RSpec.describe 'accounts' do
  context 'when not signed in' do
    let(:account) { create(:account) }

    it 'does not let you view them' do
      visit account_path(account)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content('not authorized')
    end
  end

  context 'when signed in' do
    include_context 'when signed in as a site user'

    let(:account) { create(:account_membership, user: current_user).account }

    it 'shows the account name' do
      visit account_path(account)
      expect(page).to have_content(account.name)
    end

    it 'lets you edit the account name' do
      visit account_path(account)
      click_on 'Edit Account'

      fill_in 'Account Name', with: 'my family account'
      click_on 'Update'

      expect(page).to have_current_path(account_path(account))
      expect(page).to have_content('Account Updated')
      expect(page).to have_content('my family account')
    end

    it 'refuses an empty account name' do
      visit account_path(account)
      click_on 'Edit Account'

      fill_in 'Account Name', with: ''
      click_on 'Update'

      expect(page).to have_current_path(account_path(account))
      expect(page).to have_content('Account update failed')
      expect(page).to have_content("Account Name can't be blank")
    end
  end
end
