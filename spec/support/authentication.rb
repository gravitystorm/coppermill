RSpec.shared_context 'when signed in' do
  before do
    visit new_user_session_path
    fill_in 'Email', with: current_user.email
    fill_in 'Password', with: password
    click_button 'Log in'
    expect(page).to have_content('Signed in successfully')
    expect(page).to have_current_path(dashboard_path)
  end
end

RSpec.shared_context 'when signed in as a site user', as: :site_user do
  include_context 'when signed in'

  let(:current_user) { create(:confirmed_user) }
  let(:password) { attributes_for(:confirmed_user)[:password] }
end
